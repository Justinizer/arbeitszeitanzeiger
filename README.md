# Arbeitszeitanzeiger

## Installation

1. Tampermonkey (Chrome und Opera) oder Greasemonkey (Firefox) installieren
2. [Diese URL](https://gitlab.com/jloewe/arbeitszeitanzeiger/raw/master/arbeitszeitanzeiger.user.js) öffnen und das Skript installieren
3. [PTB Zeus öffnen](https://p-zeus.bs.ptb.de/) und ausprobieren!
