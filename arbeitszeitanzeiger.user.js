// ==UserScript==
// @name         Arbeitszeitanzeiger
// @namespace    https://jloewe.net/
// @version      0.2
// @description  Zeigt die Arbeitszeit in ZEUS an
// @author       Jan Henry Loewe
// @match        https://p-zeus.bs.ptb.de/*/Today.aspx
// @grant        none
// @require      https://code.jquery.com/jquery-3.4.1.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/de.js
// @updateURL	 https://gitlab.com/jloewe/arbeitszeitanzeiger/raw/master/arbeitszeitanzeiger.user.js
// ==/UserScript==

const m = moment;
m.locale("de")

const $ = window.$;

const defaultHours = getHoursForDay(m().format("d"));
const step = 0.25;
const minHours = 6;
const maxHours = 10;

(function() {
    'use strict';

    // add report iframe
    let frame = $(`<iframe style="display: none" src="${window.location.href.replace("Today.aspx", "Module/Report/Report.aspx")}"></iframe>`);

    frame.appendTo("body");

    let interval = setInterval(() => {
        // extract report from iframe
        let report = frame.contents().find("#FrmReportContent").contents().find("#uiReport").text();

        if(report.length > 0) {
            // report successfully loaded
            clearInterval(interval);

            let t = m();

            let reportRegex = new RegExp(`${t.format("dd")}.*${t.format("D")}.*\\d?\\d:\\d\\d -`, "gm");
            let matches = report.match(reportRegex);

            let firstBooking = matches[0];
            firstBooking = firstBooking.substring(0, firstBooking.length - 2);
            firstBooking = m(firstBooking.substring(firstBooking.length - 4), "H:mm");

            $("#firstBooking").text(firstBooking.format("H:mm [Uhr]"));

            let range = $("#hourPicker");

            range.on("input", function(){
                setLeaveTime(firstBooking, range.val());
            });

            setLeaveTime(firstBooking, range.val());
        }
    }, 400);

    addUi();
})();

function addUi() {
    $(`
<fieldset class="fieldset_base">
<legend class="base blue bold">
Arbeitszeitanzeiger
</legend>

<div style="padding:0 10px 10px 10px; max-width: 250px; margin-left: auto; margin-right: auto;">
<div style="width: 100%;">
<input style="width: 100%;" type="range" id="hourPicker" value="${defaultHours}" min="${minHours}" max="${maxHours}" step="${step}">
<span>${minHours}h</span>
<span style="float: right;">${maxHours}h</span>
<div style="clear: right;"></div>
</div>

<table style="margin-top: 20px">
<tbody>
<tr><td class="base blue bold">Erste Buchung:</td><td id="firstBooking" class="base blue bold"></td></tr>
<tr><td class="base blue bold">Arbeitszeit:</td><td id="duration" class="base blue bold"></td></tr>
<tr><td class="base blue bold">Enthaltene Pausenzeit:</td><td id="breakTime" class="base blue bold"></td></tr>
<tr><td class="base blue bold">Feierabend:</td><td id="endTime" class="base blue bold"></td></tr>
<tr><td class="base blue bold">Verbleibend:</td><td id="remainingTime" class="base blue bold"></td></tr>
</tbody>
</table>
</div>
</fieldset>`).appendTo("#uiDivMyAccounts");
}

function setLeaveTime(firstBooking, duration) {
    const endTime = m(firstBooking).add(duration, "hours");

    // add break time to endTime
    let breakTime = duration < 9 ? 30 : 45;
    endTime.add(breakTime, "minutes");

    $("#duration").text(`${Math.floor(duration)}h ${duration % 1 > 0 ? `${duration % 1 * 60}min` : ""}`);
    $("#breakTime").text(`${breakTime}min`);
    $("#endTime").text(endTime.format("H:mm [Uhr]"));
    $("#remainingTime").text(`${Math.floor(timeDifference / 3600000)}h ${Math.ceil((timeDifference % 3600000) / 60000)}min`);
}

function getHoursForDay(day) {
    switch (day) {
        // monday
        case "1":
            return 8;
        // tuesday, wednesday, thursday
        case "2":
        case "3":
        case "4":
            return 8.25;
        // friday
        case "5":
            return 6.25;
        default:
            return 8;
    }
}
